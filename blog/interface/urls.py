from django.conf.urls import patterns, include, url

from .views import post, user


urlpatterns = patterns('',
    url('^post', include(patterns('',
        url(r'^$', post.List.as_view(), name='list'),
        url(r'^/(?P<post_id>\d+)$', post.View.as_view(), name='view'),
    ), namespace='post')),
    url('^user', include(patterns('',
        url(r'^$', user.View.as_view(), name='user_view'),
    ), namespace='post')),
)
