from django.conf import settings

from utils.api_clients.backend import BackendAPIClient


class BackendMixin(object):

    @property
    def backend(self):
        token = None
        if hasattr(self.request, 'user') and 'token' in self.request.user:
            token = self.request.user['token']

        return BackendAPIClient(settings.BACKEND_API_URL, token)
