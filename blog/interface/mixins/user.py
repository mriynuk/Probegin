from django.http import HttpResponseForbidden

from utils.api_clients.backend import PostUnknownAPIClientError


class UserMixin(object):

    user = None

    def dispatch(self, *args, **kwargs):

        try:
            # todo: improve this
            if hasattr(self.request, 'user') and 'id' in self.request.user:
                # show current user
                id = self.request.user['id']
                self.user = self.backend.user.get(id)
            else:
                # todo: implement login form
                return HttpResponseForbidden('403 Forbidden')

        except PostUnknownAPIClientError:
            return HttpResponseForbidden('403 Forbidden')

        return super(UserMixin, self).dispatch(*args, **kwargs)
