import tortilla

from .exceptions import api_exception_register, APIClientError


class Resource(object):

    def __init__(self, resource):
        self._resource = resource

    @property
    def resource(self):
        return self._resource

    def all(self):
        return self.process_result(self.resource.get(headers={'this': 'that'}))

    def get(self, id):
        return self.process_result(self.resource.get(id))

    def process_result(self, result):

        if not result['success']:
            raise api_exception_register.get(result['error']['code'], APIClientError)(result['error']['message'])

        return result['result']


class Post(Resource):

    @property
    def resource(self):
        return self._resource.posts


class User(Resource):

    @property
    def resource(self):
        return self._resource.users


class BackendAPIClient(object):

    def __init__(self, api_url, token=None):
        self.api_url = api_url
        if token:
            # todo: doesn't work, investigate the reason
            self.resource = tortilla.wrap(api_url, headers={
                'HTTP_AUTHORIZATION': 'Token ' + token
            })
        else:
            self.resource = tortilla.wrap(api_url)

        self.post = Post(self.resource)
        self.user = User(self.resource)
