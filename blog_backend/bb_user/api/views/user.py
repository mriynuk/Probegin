from django.views.generic import View
from django.contrib.auth.forms import AuthenticationForm

from bb_user.api.forms.user import UserRegistrationForm
from bb_user.api.mixins import UserAPIMixin
from bb_user.api.serializers.user import serialize as serialize_user
from django.contrib.auth import get_user_model, authenticate, login, logout
from utils.api.exceptions import RequestValidationFailedAPIError
from utils.api.mixins import APIMixin

import bb_user.services.user

User = get_user_model()


class Collection(APIMixin, View):

    def get(self, request, parameters, *args, **kwargs):

        users = User.objects.all()

        return map(serialize_user, users)

    def post(self, request, parameters, *args, **kwargs):

        form = UserRegistrationForm(data=parameters)

        if not form.is_valid():
            raise RequestValidationFailedAPIError(form.errors)

        user = bb_user.services.user.create(**form.cleaned_data)

        return serialize_user(user)


class Single(APIMixin, UserAPIMixin, View):

    def get(self, request, parameters, *args, **kwargs):
        return serialize_user(self.user)


class UserLogin(APIMixin, UserAPIMixin, View):

    def post(self, request, parameters, *args, **kwargs):
        form = AuthenticationForm(data=parameters)

        if not form.is_valid():
            raise RequestValidationFailedAPIError(form.errors)

        username = parameters['username']
        password = parameters['password']
        user = authenticate(username=username, password=password)
        login(request, user)

        return serialize_user(user)

    def delete(self, request, parameters, *args, **kwargs):
        logout(request)
