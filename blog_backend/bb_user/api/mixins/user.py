from bb_user.api.exceptions import UserUnknownAPIError
from django.contrib.auth import get_user_model

User = get_user_model()


class UserAPIMixin(object):

    user = None

    def dispatch(self, *args, **kwargs):
        if 'token' in kwargs:
            token = kwargs['token']
            if token is not None and isinstance(token, bytes):
                # We will decode `token` if it is of type
                # bytes.
                kwargs['token'] = token.decode('utf-8')

        if 'user_id' in kwargs:
            try:
                self.user = User.objects.get(id=kwargs['user_id'])

            except User.DoesNotExist:
                raise UserUnknownAPIError()

        return super(UserAPIMixin, self).dispatch(*args, **kwargs)
