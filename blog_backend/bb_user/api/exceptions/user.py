from utils.api.exceptions import APIError


class UserAPIError(APIError):
    pass


class UserUnknownAPIError(UserAPIError):
    code = 'user_unknown'
