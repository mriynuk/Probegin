from django.contrib.auth import authenticate


class JWTMiddleware(object):
    """
    Authenticates against a token in the http authorization header.
    """
    authentication_header_prefix = 'Token'

    def process_request(self, request):
        """
        The `process_request` method is called on every request regardless of
        whether the endpoint requires authentication.

        `process_request` has two possible return values:

        1) `None` - We return `None` if we do not wish to authenticate. Usually
                    this means we know authentication will fail. An example of
                    this is when the request does not include a token in the
                    headers.

        2) `request.user` - We return a user when authentication is successful.

                            If neither case is met, that means there's an error
                            and we do not return anything.
        """

        # `auth_header` should be an array with two elements: 1) the name of
        # the authentication header (in this case, "Token") and 2) the JWT
        # that we should authenticate against.
        auth_header = request.META.get('HTTP_AUTHORIZATION', b'').split()
        auth_header_prefix = self.authentication_header_prefix.lower()

        if not auth_header:
            return None

        if len(auth_header) != 2:
            # Invalid token header. No credentials provided. Do not attempt to
            # authenticate.
            return None

        # The JWT library we're using can't handle the `byte` type, which is
        # commonly used by standard libraries in Python 3. To get around this,
        # we simply have to decode `prefix` and `token`. This does not make for
        # clean code, but it is a good decision because we would get an error
        # if we didn't decode these values.
        prefix = auth_header[0].decode('utf-8')
        token = auth_header[1].decode('utf-8')

        if prefix.lower() != auth_header_prefix:
            # The auth header prefix is not what we expected. Do not attempt to
            # authenticate.
            return None

        user = authenticate(token=token)
        if user:
            request.user = user
