from django.contrib.auth import get_user_model

User = get_user_model()


def create(username, email=None, password1=None, password2=None):
    return User.objects.create_user(username, email=email, password=password1)
